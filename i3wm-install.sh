#! /bin/bash
#VERSION="grep '^(VERSION)=' /etc/os-release"
#NAME="grep '^(NAME)=' /etc/os-release"
#ID=grep '^(ID)=' /etc/os-release

NAME=$(egrep '^(NAME)=' /etc/os-release)
#echo El nombre es: $NAME
VERSION=$(egrep '^(VERSION)=' /etc/os-release)
#echo La versión es: $VERSION
ID=$(egrep '^(ID)=' /etc/os-release)

if [ "$ID" == "ID=ubuntu" ]
then 
echo "Tienes Ubuntu, a continuación se instalaran los siguientes paquetes:
Alacritty, Dmenu, Git, i3-wm, i3blocks, i3lock, Lxappearance, Nitrogen, Pcmanfm"
sudo apt install alacritty dmenu git i3-wm i3blocks i3lock lxappearance nitrogen pcmanfm
fi

if [ "$ID" == "ID=linuxmint" ]
then 
echo "Tienes Linux Mint, a continuación se instalaran los siguientes paquetes:
Alacritty, Dmenu, Git, i3-wm, i3blocks, i3lock, Lxappearance, Nitrogen, Pcmanfm"
sudo apt install alacritty dmenu git i3-wm i3blocks i3lock lxappearance nitrogen pcmanfm
fi

if [ "$ID" == "ID=debian" ]
then 
echo "Tienes Debian, a continuación se instalaran los siguientes paquetes:
Alacritty, Dmenu, Git, i3-wm, i3blocks, i3lock, Lxappearance, Nitrogen, Pcmanfm"
su 
apt install alacritty dmenu git i3-wm i3blocks i3lock lxappearance nitrogen pcmanfm 
fi

if [ "$ID" == "ID=fedora" ]
then 
echo "Tienes Fedora, a continuación se instalaran los siguientes paquetes:
Alacritty, Dmenu, Git, i3, i3blocks, i3lock, Lxappearance, Nitrogen, Pcmanfm"
sleep 4s
sudo dnf install alacritty dmenu git i3 i3blocks i3lock lxappearance nitrogen pcmanfm
fi

if [ "$ID" == "ID=arch" ]
then 
echo "Tienes Arch, a continuación se instalaran los siguientes paquetes:
Alacritty, Dmenu, Git, i3-wm, i3blocks, i3lock, Lxappearance, Nitrogen, Pcmanfm"
sleep 4s
sudo pacman -S alacritty dmenu git i3-wm i3blocks i3lock lxappearance nitrogen pcmanfm
fi

if [ "$ID" == "ID=manjaro" ]
then
echo "Tienes Manjaro, a continuación se instalaran los siguientes paquetes:
Alacritty, Dmenu, Git, i3-wm, i3blocks, i3lock, Lxappearance, Nitrogen, Pcmanfm"
sleep 4s
sudo pacman -S alacritty dmenu git i3-wm i3blocks i3lock lxappearance nitrogen pcmanfm
fi

echo "Copiando configuración..."

cd ~/.config
git clone https://github.com/FranxLRM/i3wm-config.git
cd i3wm-config/
cp -r alacritty/ ~/.config
cp -r i3/ ~/.config
cp -r i3blocks/ ~/.config
echo "Listo"
echo "Cierra sesión y vuelve a iniciar pero con i3"
echo "Bye :))"
